from django.shortcuts import render
from .models import Schedule
from .forms import ScheduleForm
from django.views.generic.edit import DeleteView

# Create your views here.
def home(request):
    return render(request, 'home.html')

def hobbies(request):
    return render(request, 'hobbies.html')

def schedule(request):
    #Cek apakah request berasal dari halaman form di mana request tersebut memiliki method POST
    #Karena bisa saja request berasal dari halaman lain(misal profile, dll)
    if request.method == "POST":
        #memasukkan field yang telah terisi mjd instance dr ScheduleForm
        form = ScheduleForm(request.POST)
        #Jika input dari user valid
        if form.is_valid():
            form.save()
        else:
            form = ScheduleForm()
            arguments = {
                'form' : form,
            }
            return render(request, 'schedule_form.html',arguments)

    arguments = {
        'schedules': Schedule.objects.all(),
    }
    return render(request, 'schedule.html', arguments)

def schedule_form(request):
    #Membuat instance dari form yg masih kosong
    form = ScheduleForm()
    arguments = {
        'form' : form,
    }
    return render(request, 'schedule_form.html',arguments)


def delete_schedule(request, id):
    unschedule = Schedule.objects.filter(id=id)[0]
    unschedule.delete()
    arguments = {
        'schedules': Schedule.objects.all(),
    }
    return render(request,'schedule.html',arguments)



    
