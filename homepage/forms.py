from django import forms
from .models import Schedule

class DateInput(forms.DateInput):
    input_type = 'date'


class TimeInput(forms.TimeInput):
    input_type = 'time'

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = ['day', 'date','time','activities', 'place', 'categories']
        widgets = {
            'day' : forms.Select(attrs={
					'class': 'form-control'
					}),
            'date' : DateInput(attrs={
					'class': 'form-control'
					}),
            'time' : TimeInput(attrs={
					'class': 'form-control'
					}),
            'activities' : forms.TextInput(attrs={
					'class': 'form-control'
					}),
            'place' : forms.TextInput(attrs={
					'class': 'form-control'
					}),
            'categories' : forms.Select(attrs={
					'class': 'form-control'
					}),
        }