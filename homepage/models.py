from django.db import models
from django.forms import ModelForm

days = [
    ('Mon', 'Monday'),
    ('Tue', 'Tuesday'),
    ('Wed', 'Wednesday'),
    ('Thu', 'Thursday'),
    ('Fri', 'Friday'),
    ('Sat', 'Saturday'),
    ('Sun', 'Sunday'),
]


activites_categories = [
    ('Academic', 'Academic'),
    ('Organization', 'Organization'),
    ('Committee', 'Committee'),
]

class Schedule(models.Model):
    day = models.CharField(max_length=20, choices=days)
    date = models.DateField()
    time = models.TimeField()
    activities = models.CharField(max_length=200)
    place = models.CharField(max_length=100)
    categories = models.CharField(max_length=20, choices=activites_categories)
