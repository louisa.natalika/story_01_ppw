# Generated by Django 2.1.1 on 2019-10-05 15:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0002_auto_20191005_2230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='categories',
            field=models.CharField(blank=True, choices=[('Academic', 'Academic'), ('Organization', 'Organization'), ('Committee', 'Committee')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
