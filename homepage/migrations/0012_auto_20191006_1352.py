# Generated by Django 2.1.1 on 2019-10-06 06:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0011_auto_20191006_1329'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='schedule',
            name='date',
        ),
        migrations.AlterField(
            model_name='schedule',
            name='activities',
            field=models.CharField(max_length=250),
        ),
    ]
