from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('hobbies/', views.hobbies, name='hobbies'),
    path('schedule/', views.schedule, name='schedule'),
    path('add_schedule/', views.schedule_form, name='schedule_form'),
    path('delete_schedule/<id>', views.delete_schedule, name='delete_schedule'),
]